import {Results} from "./Results";

export interface Generation{
  count: number;
  results: Results[];
}
