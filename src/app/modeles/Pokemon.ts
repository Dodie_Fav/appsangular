import {Sprites} from "./Sprites";

export interface Pokemon{
 name: string;
 sprites: Sprites;
}
