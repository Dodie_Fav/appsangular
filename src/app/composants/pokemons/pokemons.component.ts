import {Component, Inject} from '@angular/core';
import {Pokemon} from "../../modeles/Pokemon";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.scss']
})
export class PokemonsComponent {
 // public pokemons: Pokemon[];

public pokemons: Observable<Pokemon>[];
  constructor(
    @Inject(HttpClient) private  http : HttpClient
  ) {
    this.pokemons = [];
    for (let i = 450; i < 500 ; i++) {
      let pokemon$ = this.http.get<Pokemon>('https://pokeapi.co/api/v2/pokemon/' +i);
      this.pokemons.push(pokemon$)
    }


    // this.pokemons = [
    //   new Pokemon(1,"Bulbizarre", "Graine", "Plante, Poison", 0.7,6.9),
    //   new Pokemon(2,"Herbizarre", "Graine","Plante, Poison", 1,13),
    //   new Pokemon(3,"Florizarre", "Graine","Plante, Poison", 2,100),
    //   new Pokemon(4,"Salamèche", "Lézard","Feu", 0.6,8.5),
    //   new Pokemon(5,"Reptincel", "Flamme","Feu", 1.1,19),
    //   new Pokemon(6,"Dracaufeu", "Flamme","Feu, Vol", 1.7,90.5)
    // ];
  }
}
