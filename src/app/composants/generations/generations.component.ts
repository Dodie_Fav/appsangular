import {Component, Inject} from '@angular/core';
import {Generation} from "../../modeles/Generation";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Results} from "../../modeles/Results";

@Component({
  selector: 'app-generations',
  templateUrl: './generations.component.html',
  styleUrls: ['./generations.component.scss']
})
export class GenerationsComponent {
  public generations$: Observable<Generation>;


  constructor(
    @Inject(HttpClient) private  http : HttpClient
  ) {
    this.generations$ = this.http.get<Generation>('https://pokeapi.co/api/v2/generation/');

  }
}
